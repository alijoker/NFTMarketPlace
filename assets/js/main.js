
gsap.registerPlugin(ScrollTrigger);
let container = document.getElementById("container")
let Sections = gsap.utils.toArray("section");
let gallery = gsap.utils.toArray("gallery");


let container1 = document.getElementById("container1")
let Sections2 = gsap.utils.toArray(".sectionTeam");


// for scroll horizontally on canvas
//----------------------------------
gsap.to(container, {
  xPercent: -100 * (Sections.length + 0),
  ease: "ease-in-out",
  scrollTrigger: {
    trigger: container,
    scrub: true,
    pin:'.scroll',
    // pin:true
    start:"top top",
    end: `+=${container.offsetWidth}`
    
  }
})

// for responsive positioning in mobile devices
//---------------------------------------------
var teamSection 
var galleryItems1  
var galleryItems2  
if (window.window.innerWidth > 769){
  teamSection = 2
  var galleryItems1 = -300 
  var galleryItems2 = +300 

}else if (window.innerWidth < 768){ 
  teamSection = 2
  var galleryItems1 = -300 
  var galleryItems2 = -300 
}else if (window.innerWidth < 568){ 
  teamSection = 3
  var galleryItems1 = -300 
  var galleryItems2 = -300 
}

gsap.to(container1, {
  xPercent: -100 * (Sections2.length + teamSection),
  ease: "ease-in-out",
  scrollTrigger: {
    trigger: container1,
    scrub: true,
    pin:'.teamDark',
    // pin:true
    // markers:true,
    start:"top top",
    end: `+=${container1.offsetWidth}`
    
  }
})

// for changing background color gallery
// --------------------------------------------------------

 ScrollTrigger.create({
    trigger:  "body",
    start: "13% center",
    end: "74% end",
    // markers: true,
    toggleClass: "galleryDark",
    
  })

  ScrollTrigger.create({
    trigger:  "body",
    start: "74% 20%",
    end: "100% end",
    // markers: true,
    toggleClass: "teamLight",
   
  })

  // for title and description in header
  // ----------------------------
  gsap.to("#titleHeaders",{
    scrollTrigger:{
      trigger : "#titleHeaders",
      start: "top 30%",
      scrub: true,
      
    },
    y:-100,
    duration: 3,
  })

  gsap.to("#paragraphHeaders",{
    scrollTrigger:{
      trigger : "#paragraphHeaders",
      start: "top center",
      scrub: true,
    },
    opacity: 0
  })


// for gallery effects 
// --------------------------------
gsap.to(".firstItems",{
  scrollTrigger:{
    trigger : ".firstItems",
    start: "top 30%",
    scrub: true,
    // markers: true,
    // markers:{
    //       startColor: "pink", endColor: "pink",
    //     }
  },
  y:galleryItems1,
  duration: 3,
})

gsap.to(".lastItems",{
  scrollTrigger:{
    trigger : ".lastItems",
    start: "top 30%",
    scrub: true,
    // markers: true,
    // markers:{
    //       startColor: "pink", endColor: "pink",
    //     }
  },
  y:galleryItems2,
  duration: 3,
})
